import unittest

from RomanConverter import RomanConverter


class MyTestCase(unittest.TestCase):
    def test_0_raises_value_error(self):
        with self.assertRaises(ValueError):
            RomanConverter().arabic_to_roman(0)

    def test_3001_raises_value_error(self):
        with self.assertRaises(ValueError):
            RomanConverter().arabic_to_roman(3001)

    def test_1_return_I(self):
        assert RomanConverter().arabic_to_roman(1) == "I"

    def test_3_return_III(self):
        assert RomanConverter().arabic_to_roman(3) == "III"

    def test_4_return_IV(self):
        assert RomanConverter().arabic_to_roman(4) == "IV"

    def test_10_return_X(self):
        assert RomanConverter().arabic_to_roman(10) == "X"

    def test_21_return_XXI(self):
        assert RomanConverter().arabic_to_roman(21) == "XXI"

    def test_153_return_XXI(self):
        assert RomanConverter().arabic_to_roman(153) == "CLIII"

    def test_2463_return_XXI(self):
        assert RomanConverter().arabic_to_roman(2463) == "MMCDLXIII"

    def test_I_return_1(self):
        assert RomanConverter().roman_to_arabic("I") == 1

    def test_V_return_5(self):
        assert RomanConverter().roman_to_arabic("V") == 5

    def test_X_return_10(self):
        assert RomanConverter().roman_to_arabic("X") == 10

    def test_L_return_50(self):
        assert RomanConverter().roman_to_arabic("L") == 50

    def test_C_return_100(self):
        assert RomanConverter().roman_to_arabic("C") == 100

    def test_D_return_500(self):
        assert RomanConverter().roman_to_arabic("D") == 500

    def test_M_return_1000(self):
        assert RomanConverter().roman_to_arabic("M") == 1000

    def test_2digit_II_return_2(self):
        assert RomanConverter().roman_to_arabic("II") == 2

    def test_2digit_IV_return_4(self):
        assert RomanConverter().roman_to_arabic("IV") == 4

    def test_2digit_IX_return_9(self):
        assert RomanConverter().roman_to_arabic("IX") == 9

    def test_2digit_XL_return_40(self):
        assert RomanConverter().roman_to_arabic("XL") == 40

    def test_digits_XLIX_return_49(self):
        assert RomanConverter().roman_to_arabic("XLIX") == 49

    def test_digits_XC_return_90(self):
        assert RomanConverter().roman_to_arabic("XC") == 90

    def test_digits_XCIV_return_94(self):
        assert RomanConverter().roman_to_arabic("XCIV") == 94

    def test_digits_CD_return_400(self):
        assert RomanConverter().roman_to_arabic("CD") == 400

    def test_digits_CM_return_900(self):
        assert RomanConverter().roman_to_arabic("CM") == 900

    def test_converter_validator(self):
        for num in range(1, 3001):
            roman = RomanConverter().arabic_to_roman(num)
            arabic = RomanConverter().roman_to_arabic(roman)
            #print(arabic, ' ==> ', roman)
            assert num == arabic


if __name__ == '__main__':
    unittest.main()
