def converter(digit, high, med, low):
    if digit == 0:
        return ""
    cases = {1: low, 2: 2 * low, 3: 3 * low,
             4: low + med, 5: med, 6: med + low,
             7: med + 2 * low, 8: med + 3 * low, 9: low + high}
    return cases[digit]


class RomanConverter(object):
    def __init__(self):
        self.arabic = 0

    def is_valid_digit(self, digit):
        if digit <= 0 or digit > 3000:
            raise ValueError
        return True

    def arabic_to_roman(self, arabic):
        if self.is_valid_digit(arabic):
            self.arabic = arabic

        thousands = self.to_roman(1000, "", "", "M")

        hundreds = self.to_roman(100, "M", "D", "C")

        tens = self.to_roman(10, "C", "L", "X")

        unit = converter(self.arabic, "X", "V", "I")

        return thousands + hundreds + tens + unit

    def to_roman(self, divisor, high, med, low):
        arabic_digit = self.arabic // divisor
        if arabic_digit != 0:
            self.arabic -= divisor * arabic_digit
        return converter(arabic_digit, high, med, low)

    def roman_to_arabic(self, romans):
        arabic = 0
        i = 0
        while i < len(romans):

            for array in [[1, ["I", "V", "X"]], [10, ["X", "L", "C"]], [100, ["C", "D", "M"]]]:
                if romans[i] == array[1][0]:
                    if i + 1 < len(romans):
                        if romans[i + 1] == array[1][1]:
                            arabic += array[0] * 4
                        elif romans[i + 1] == array[1][2]:
                            arabic += array[0] * 9
                        else:
                            arabic += array[0] * 1
                            i += 1
                            break
                        i += 2
                        break
                    else:
                        arabic += array[0] * 1
                        i += 1
                        break

            if i >= len(romans):
                break

            for roman in [("V", 5), ("L", 50), ("D", 500), ("M", 1000)]:
                if romans[i] == roman[0]:
                    arabic += roman[1]
                    i += 1
                    break
        return arabic

# for num in range(1, 3000):
#     print(num, ' ==> ', RomanConverter().arabic_to_roman(num))
